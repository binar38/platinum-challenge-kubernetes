#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE TABLE users (
        id SERIAL PRIMARY KEY,
        name varchar(30),
        email varchar(30),
        gender varchar(6)
    );

    INSERT INTO users (name, email, gender) VALUES
        ('John Doe', 'john@gmail.com', 'Male'),
        ('Mark Lee', 'mlee@gmail.com', 'Male'),
        ('Sofia', 'sofia@gmail.com', 'Female'),
        ('Michelle', 'mangela@gmail.com', 'Female');
EOSQL