# Fullstack JS (reactjs, expressjs, postgresql)

  

Simple CRUD app built with ReactJS, ExpressJS, PostgreSQL.

  

## Staging

```

Database : postgresql

Backend : staging.richardkhonan.site/users

Frontend : staging.richardkhonan.site

```


## Production

```

Database : postgresql

Backend : production.richardkhonan.site/users

Frontend : production.richardkhonan.site

```

## Diagram

 - [Architecture diagram](https://excalidraw.com/#json=wX5PRM18Nq_W03vWcZOHT,mHJsPaOqYr2uPQh5khJ_4w)

## Monitoring

![plot](./assets/Screenshot 2023-04-04 235329.png)
![plot](./assets/Screenshot%202023-04-04%20235403.png)

## Logging

![plot](./assets/Screenshot%202023-04-04%20235647.png)