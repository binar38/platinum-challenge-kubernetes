resource "google_sql_database_instance" "db_instance" {
  name             = "binar-platinum-db"
  database_version = "POSTGRES_14"
  region           = "asia-southeast2"

  settings {
    tier = "db-f1-micro"
    availability_type = "ZONAL"
    disk_size = 10
    activation_policy = "ALWAYS"
    deletion_protection_enabled = false
    ip_configuration {
      authorized_networks {
        name = "Allow Internet"
        value = "0.0.0.0/0"
      }
    }
  }
}

resource "google_sql_database" "database" {
  name = "people"
  instance = google_sql_database_instance.db_instance.name
}

resource "google_sql_database" "database_staging" {
  name = "people-1"
  instance = google_sql_database_instance.db_instance.name
}

resource "google_sql_user" "users" {
  name = "people"
  instance = google_sql_database_instance.db_instance.name
  password = random_id.random_password.hex
}
