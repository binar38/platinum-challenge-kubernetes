resource "google_compute_instance" "platinum-challenge" {
    name = "platinum-gitlab-runner"
    machine_type = "e2-micro"
    boot_disk {
      initialize_params {
        image = "ubuntu-os-cloud/ubuntu-2204-lts"
      }
    }
    metadata = {
      ssh-keys = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCiwAsjKGrplNHT6EbwYHeFxdDdZtXmzu1/YaFYoHidyWo3yLTpmnLkR5lYWG83De9CDl5BUfYfYo1LJgClOZWCN7D51rJSdAuJeEfXfZW+cL+k+W0y5F67Llp73RgabXfTr8k5pOivx5AXckdBWYyMgnv+NwmAwW+/XnJ9UrQB/GfwG9RbsJ9g72h5jYb+OgL7YS1z29DLAg3n69Bh9RwHMtOGH+pAF/1XjbPmaGrxjJ+Fr3Zsz+n+lT2pUjJ1GUeZhFCAZlMDin8/cLpXO9ZyndSc5hkO4yTmy463XMUOD5XlsVZU3xXCbSpEIQhP2qzFICir2Lv4DhnNYMc+fj4rXXKjKxfN+AKqbsawHUHL18g4Nvr1VIPDfxyfGY+yDmeji1Frym1U0C3M2G/VUsE37mTjLeGHp+4/byU7cHKeGpNY3xQIZxiQt9i3Cynbzs5wSnccA6Hyp+jPncYDZA4bnkvbt9UbpyFd69aXeZrLaqcJUpL4OqY/x6EVkdCERbk= richard@richard"
    }
    tags = ["http-server"]
    network_interface {
      network = "default"
      access_config {

      }
    }
}
