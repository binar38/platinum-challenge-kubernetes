provider "google" {
  project     = "titanium-flash-381109"
  region      = "asia-southeast2"
  zone        = "asia-southeast2-a"
}

terraform {
 backend "gcs" {
   bucket  = "terraform-state-platinum"
   prefix  = "terraform/state"
 }
}