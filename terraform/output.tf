output "out_sql_private_ip" {
  description = "Expose private IP dari SQL instance"
  value = google_sql_database_instance.db_instance.private_ip_address
}

output "out_sql_public_ip" {
  description = "Expose public IP dari SQL instance"
  value = google_sql_database_instance.db_instance.public_ip_address
}

output "out_sql_password" {
  description = "Sql user password"
  value = random_id.random_password.hex
}